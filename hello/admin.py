# django modules
from django.http import HttpResponseRedirect

# google modules
from google.appengine.api import users

# project modules and settings
from myproject import settings

def login_required(func):
    # check if user is logged in
    def _wrapper(request, *args, **kwargs):
        user = users.get_current_user()
        if user:
            return func(request, *args, **kwargs)
        else:
            return HttpResponseRedirect(users.create_login_url(request.get_full_path()))

    return _wrapper

def admin_required(func):
    # check if user has admin permission
    def _wrapper(request, *args, **kwargs):
        if is_admin():
            return func(request, *args, **kwargs)
        else:
            return HttpResponseRedirect(users.create_login_url(request.get_full_path()))

    return _wrapper

def is_admin():
    # only users in AUTHORS list have admin permission
    user = users.get_current_user()
    if user and user.email() in settings.AUTHORS:
        return True
    else:
        return False
