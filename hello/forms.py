# django modules
from django import forms as newforms

# google modules, for some strange reasons gae doesn't provide this module??
# from google.appengine.ext.db import djangoforms as forms

# project modules
import djangoforms as forms
from models import Article

class ArticleForm(forms.ModelForm):
    """
    Article form
    """
    title = newforms.CharField(label=u'Title', widget=newforms.TextInput)
    class Meta:
        model = Article
        exclude = ['author']
