# google modules
from google.appengine.ext import db

# Create your models here.

class Article(db.Model):
    """
    Article class
    """
    author = db.UserProperty()
    title = db.StringProperty(required=True)
    content = db.TextProperty(required=True)
    create_time = db.DateTimeProperty(auto_now_add=True)
    update_time = db.DateTimeProperty(auto_now=True)
    is_published = db.BooleanProperty()

    def get_absolute_url(self):
        return '/article/%s/' %self.key().id()

