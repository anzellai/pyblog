# django module
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.template.context import RequestContext

# google module
from google.appengine.api import users
#from google.appengine.ext import ndb

# project module and settings
#from myproject import settings
from forms import ArticleForm
from models import Article
from admin import login_required, admin_required , is_admin

def login(request):
    # basic login view
    return HttpResponseRedirect(users.create_login_url('/'))

def logout(request):
    # basic logout view
    return HttpResponseRedirect(users.create_logout_url('/'));

def home(request):
    return HttpResponseRedirect('/article/list_article')

def get_articles():
    # filter only published articles if user is not admin
    articles = Article.all()
    articles.order('-create_time')
    if not is_admin():
        articles.filter('is_published', True)
    return articles

@admin_required
def add_article(request):
    articles = get_articles()
    # add article
    if request.method == 'GET':
        form = ArticleForm()

    if request.method == 'POST':
        form = ArticleForm(request.POST)
        if form.is_valid():
            article = form.save(commit=False)
            article.author = users.get_current_user()
            article.put()
            return HttpResponseRedirect('/article/list_article')

    user = users.get_current_user()
    template_values = {
        'form': form,
        'articles': articles,
        'user': user,
        'is_admin': is_admin()
    }
    return render_to_response('hello/add_article.html',
                              template_values,
                              context_instance=RequestContext(request))

def list_article(request):
    # list all article
    user = users.get_current_user()
    articles = get_articles()
    template_values = {
        'articles': articles,
        'user': user,
        'is_admin': is_admin()
    }
    return render_to_response('hello/list_article.html',
                              template_values,
                              context_instance=RequestContext(request))

@login_required
def view_article(request, article_id):
    # view single article
    article = Article.get_by_id(int(article_id))
    if not article:
        raise Http404
    if not article.is_published and not is_admin():
        raise Http404
    articles = get_articles()
    user = users.get_current_user()
    template_values = {
        'article': article,
        'articles': articles,
        'user': user,
        'is_admin': is_admin()
    }
    return render_to_response('hello/view_article.html',
                              template_values,
                              context_instance=RequestContext(request))

@admin_required
def edit_article(request, article_id):
    # edit and update single article
    article = Article.get_by_id(int(article_id))
    if not article:
        raise Http404
    articles = get_articles()
    if request.method == 'GET':
        form = ArticleForm({'title': article.title,
                            'content': article.content,
                            'is_published': article.is_published})
    if request.method == 'POST':
        form = ArticleForm(request.POST)
        if form.is_valid():
            modified_article = form.save(commit=False)
            article.title = modified_article.title
            article.content = modified_article.content
            article.is_published = modified_article.is_published
            article.put()
            return HttpResponseRedirect('/article/%s/' %article.key().id())
    user = users.get_current_user()
    template_values = {
        'form': form,
        'articles': articles,
        'user': user,
        'is_admin': is_admin()
    }
    return render_to_response('hello/edit_article.html',
                              template_values,
                              context_instance=RequestContext(request))

@admin_required
def delete_article(request, article_id):
    # delete single article
    article = Article.get_by_id(int(article_id))
    if not article:
        raise Http404
    article.delete()
    return HttpResponseRedirect('/article/list_article')

def handle_404(request):
    # 404 page handler
    page_title = 'Page Not Found'
    return render_to_response('404.html', locals(),
                              context_instance=RequestContext(request))

def handle_500(request):
    # 500 page handler
    page_title = 'Server Error'
    return render_to_response('500.html', locals(),
                              context_instance=RequestContext(request))

