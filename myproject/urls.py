from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'hello.views.home'),
)

urlpatterns += patterns('hello.views',
    (r'^article/login/$', 'login'),
    (r'^article/logout/$', 'logout'),
    (r'^article/add_article/$', 'add_article'),
    (r'^article/list_article/$', 'list_article'),
    (r'^article/(?P<article_id>\d+)/$', 'view_article'),
    (r'^article/(?P<article_id>\d+)/edit/$', 'edit_article'),
    (r'^article/(?P<article_id>\d+)/delete/$', 'delete_article'),
)

hanlder404 = 'hello.views.handle_404'
hanlder500 = 'hello.views.handle_500'
